import logo from './logo.svg';
import './App.css';

import Header from '../src/components/header/Header';
import Nav from '../src/components/nav/Nav';
import About from '../src/components/about/About';
import Experience from '../src/components/experience/Experience';
import Playground from './components/playground/Playground';
import Contact from '../src/components/contact/Contact';
import Footer from '../src/components/footer/Footer';
import Porfolio from './components/porfolio/Portfolio';


const App =() => {
    return (
        <>
            <Header />
            <Nav />
            <About />
            <Experience />
            <Porfolio />
            <Playground />
            <Contact />
            <Footer />
        </>
    );
}

export default App;
