import React from 'react'
import './about.css'

import ME from '../../assets/mee-about.jpg'
import {FaAward} from 'react-icons/fa';
import {VscFolderLibrary} from 'react-icons/vsc';



const About = () => {

    

    return (
        <section id='about' >
            <h5>Get To Know</h5>
            <h2>About Me</h2>

            <div className='container about__container' >
                <div className='about__me' >
                    <div className='about__me-image' >
                        <img src={ME} alt="About Image" />
                    </div>
                </div>
                <div className='about__content' >
                    <div className="about__cards">
                        <article className='about__card' >
                            <FaAward className='about__icon' />
                            <h5>Experience</h5>
                            <small>1.5+ Years</small>
                        </article>

                        <article className='about__card' >
                            <VscFolderLibrary className='about__icon' />
                            <h5>Projects</h5>
                            <small>5+ Completed</small>
                        </article>
                    </div>

                    
                    <p>I am Zaid Ahmed. Currently, I am pursuing my B.Tech(4th year) from Christ University, Bangalore. I am a self-taught programmer and have picked up React and React Native on my own. I have 1.5 years of experience in React Native.</p>

                    <a href='#contact' className='btn btn-primary' >Let's Talk</a>
                
                </div>
            </div>
        </section>
    )
}

export default About