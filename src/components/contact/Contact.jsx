import React, {useState, useRef} from 'react'
import './contact.css'

import {MdOutlineEmail} from 'react-icons/md'
import {RiMessengerLine} from 'react-icons/ri'
import {BsWhatsapp} from 'react-icons/bs'

import emailjs from '@emailjs/browser';

const Contact = () => {

    const form = useRef()
    
    const sendEmail = (e) => {
        e.preventDefault();
    
        emailjs.sendForm('service_wmefj79', 'template_vvmpklp', form.current, 'LCJTYme9uLaJkqVes')
            .then((result) => {
                console.log(result);
            }, (error) => {
                console.log(error);
            });
        
        e.target.reset()
    };

    return (
        <section id='contact' >
            <h5>Get In Touch</h5>
            <h2>Contact Me</h2>

            <div className='container contact__container' >
                <div className='contact__options' >
                    <article className="contact__option" >
                        <MdOutlineEmail className='contact__option-icon' />
                        <h4>Email</h4>
                        <h5>mailtozaid09@gmail.com</h5>
                        <a target="_blank" href='mailto:mailtozaid09@gmail.com'>Send a message</a>
                    </article>

                    {/* <article className="contact__option" >
                        <RiMessengerLine className='contact__option-icon'/>
                        <h4>Messenger</h4>
                        <h5>Zaid Ahmed</h5>
                        <a target="_blank" href='https://m.me/ernest.achiever'>Send a message</a>
                    </article>

                    <article className="contact__option" >
                        <BsWhatsapp className='contact__option-icon'/>
                        <h4>Whatsapp</h4>
                        <h5>+91 9027346976</h5>
                        <a target="_blank" href='https://api.whatsapp.com/send?phone=+9972187407'>Send a message</a>
                    </article> */}

                </div>

                <form onSubmit={sendEmail} ref={form} >
                    <input type="text" name='name' placeholder='Your Full Name' required />
                    <input type="email" name='email' placeholder='Your Email' required />
                    <textarea type="message" name="message" placeholder='Your Message' required ></textarea>
                    <button type='submit' className='btn btn-primary' >Send Message</button>
                </form>

            </div>
                
        </section>
    )
}

export default Contact