import React from 'react'
import './header.css'

import {BsLinkedin} from 'react-icons/bs';
import {FaGithub} from 'react-icons/fa';
import {BsTwitter} from 'react-icons/bs';


const HeaderSocials = () => {
    return (
        <div className='header__socials' >
            <a href='https://www.linkedin.com/in/zaid-ahmed-3175861a7/' target="_blank" > <BsLinkedin /> </a>
            <a href='https://github.com/ahmed5605' target="_blank" >  <FaGithub /> </a>
            <a href='https://twitter.com/zaidahmed09' target="_blank" >  <BsTwitter /> </a>
        </div>
    )
}

export default HeaderSocials 

