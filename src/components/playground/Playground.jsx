import React,{useState} from 'react'

import './playground.css'

import  IMG1 from '../../assets/portfolio1.jpg';
import  IMG2 from '../../assets/portfolio2.jpg';
import  IMG3 from '../../assets/portfolio3.jpg';
import  IMG4 from '../../assets/portfolio4.jpg';
import  IMG5 from '../../assets/portfolio5.png';
import  IMG6 from '../../assets/portfolio6.jpg';

import DragNDrop from '../../assets/dragndrop.mp4'
import MemoryTap from '../../assets/memorytap.mp4'

// import  DragNDropImg from '../../assets/draggame.png';
// import  MemoryTapImg from '../../assets/tapgame.png';

import  DragNDropImg from '../../assets/drag.png';
import  MemoryTapImg from '../../assets/tap.png';



import DragVideo from '../../assets/mock2.mp4'
import TapVideo from '../../assets/tap_video.mp4'
import EshopVideo from '../../assets/eshop_video.mp4'
import NotesVideo from '../../assets/notes_video.mp4'


import Modal from 'react-modal';

import ReactPlayer from 'react-player'

const data = [
    {
        id: 1,
        image: DragNDropImg,
        title: 'Drag & Drop',
        github: 'https://github.com',
        demo: DragNDrop,
    },
    {
        id: 2,
        image: MemoryTapImg,
        title: 'Memory Tap Game',
        github: 'https://github.com',
        demo: MemoryTap
    },
] 

const Porfolio = () => {

    const [modalIsOpen, setIsOpen] = useState(false);

    const [demo, setDemo] = useState('');



  function openModal(url) {
    setDemo(url)
    setIsOpen(true);
  }

  function afterOpenModal() {
    // references are now sync'd and can be accessed.
    
  }

  function closeModal() {
    setIsOpen(false);
  }

    return (
        <section id='playground' >

             <h5>My Recent Work's Demo</h5>
            <h2>Playground</h2>
           

            <div className='container video__container' >
               

                <video width={'100%'} loop="true" autoplay="autoplay"  muted>
                    <source src={DragVideo} type="video/mp4"/>
                </video> 

                <video width={'100%'} loop="true" autoplay="autoplay"  muted>
                    <source src={TapVideo} type="video/mp4"/>
                </video> 

                <video width={'100%'} loop="true" autoplay="autoplay"  muted>
                    <source src={EshopVideo} type="video/mp4"/>
                </video> 

                <video width={'100%'} loop="true" autoplay="autoplay"  muted>
                    <source src={NotesVideo} type="video/mp4"/>
                </video> 
                {/* <video width={'100%'} loop="true" autoplay="autoplay"  muted>
                    <source src={require('../../assets/mock.mp4')} type="video/mp4"/>
                </video> 

                <video width={'100%'} loop="true" autoplay="autoplay"  muted>
                    <source src={require('../../assets/mock.mp4')} type="video/mp4"/>
                </video>  */}

            </div>
            
        </section>
    )
}

export default Porfolio