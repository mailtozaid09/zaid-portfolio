import React,{useState} from 'react'

import './portfolio.css'

import  IMG1 from '../../assets/portfolio1.jpg';
import  IMG2 from '../../assets/portfolio2.jpg';
import  IMG3 from '../../assets/portfolio3.jpg';
import  IMG4 from '../../assets/portfolio4.jpg';
import  IMG5 from '../../assets/portfolio5.png';
import  IMG6 from '../../assets/portfolio6.jpg';

import DragNDrop from '../../assets/dragndrop.mp4'
import MemoryTap from '../../assets/memorytap.mp4'

// import  DragNDropImg from '../../assets/draggame.png';
// import  MemoryTapImg from '../../assets/tapgame.png';

import  DragNDropImg from '../../assets/drag.png';
import  MemoryTapImg from '../../assets/tap.png';

import Modal from 'react-modal';

import ReactPlayer from 'react-player'

const data = [
    {
        id: 1,
        image: DragNDropImg,
        title: 'Drag & Drop',
        github: 'https://github.com/ahmed5605/',
        demo: DragNDrop,
    },
    {
        id: 2,
        image: MemoryTapImg,
        title: 'Memory Tap Game',
        github: 'https://github.com/ahmed5605/',
        demo: MemoryTap
    },
] 

const Porfolio = () => {

    const [modalIsOpen, setIsOpen] = useState(false);

    const [demo, setDemo] = useState('');



  function openModal(url) {
    setDemo(url)
    setIsOpen(true);
  }

  function afterOpenModal() {
    // references are now sync'd and can be accessed.
    
  }

  function closeModal() {
    setIsOpen(false);
  }

    return (
        <section id='portfolio' >

             <h5>My Recent Work</h5>
            <h2>Porfolio</h2>
           
            <div className='container porfolio__container' >
                
                {data.map((item) => (
                    <article className='porfolio__item' >
                        <div className='porfolio__item-image' >
                            <img src={item.image} alt="" className='item-image' />
                        </div>
                        
                        <h3>{item.title}</h3>

                        <div className='porfolio__item-cta' >
                            <a href={item.github} className='btn'  target='_blank'>Github</a>
                            <a 
                            //</div>
                            href="#playground"
                           // onClick={() => openModal(item.demo)}
                            className='btn btn-primary' >Demo</a>
                        </div>
                    </article>
                ))}
                
            </div>
            {/* <Modal
                isOpen={modalIsOpen}
                onAfterOpen={afterOpenModal}
                onRequestClose={closeModal}
                className="Modal"
                overlayClassName="Overlay"
                contentLabel="Example Modal"
            >
                <div className='close-button' >
                    <div></div>
                    <div className='margin-right' >
                        <a onClick={closeModal} className='btn btn-primary'>Cancel</a>
                    </div>
                </div>
                <video width="300" height="500" autoPlay loop className='videoContainer' >
                    <source src={demo} type="video/mp4"/>
                </video>
            </Modal> */}
           
        </section>
    )
}

export default Porfolio